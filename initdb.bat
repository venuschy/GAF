java -version
set url=jdbc:postgresql://localhost:5432/postgres
set user=postgres
set password=root
set cl=classpath:com/supermap/gaf/base/data/entry/gaf-cloud-base.xml
set args=-url %url% -user %user% -password %password% -cl %cl%
java -classpath "./gaf-cloud/gaf-commons/gaf-common-base-data/target/*;./initdb.jar" com.supermap.gaf.db.Main %args%
