#!/usr/bin/env bash
java -version
export url=jdbc:postgresql://localhost:5432/postgres
export user=postgres
export password=root
export cl=classpath:com/supermap/gaf/base/data/entry/gaf-storage.xml
export args="-url $url -user $user -password $password $cl"
echo $arg
java -classpath "./gaf-cloud/gaf-commons/gaf-common-base-data/target/*:./initdb.jar" com.supermap.gaf.db.Main $args
