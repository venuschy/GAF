# 本地开发
## package
 mvn clean package -Dmaven.test.skip=true
## 初始化数据库数据
GAF使用postgres数据库，并且使用[liquibase](https://www.liquibase.org/get-started/quickstart)管理数据库脚本与更改，liquibase changeLog入口文件是[./gaf-cloud/gaf-commons/gaf-common-base-data/com/supermap/gaf/base/data/entry/gaf-cloud-base.xml](com/supermap/gaf/base/data/entry/gaf-cloud-base.xml)
您可以[安装liquibase](https://docs.liquibase.com/install/liquibase-windows.html)使用liquibase初始化数据库，或者使用GAF提供的[./initdb.bat](./initdb.bat)
### <em>使用initdb.bat初始化数据库</em>
initdb.bat文件使用classpath环境中的changeLog也就是[gaf-common-base-data](./gaf-cloud/gaf-commons/gaf-common-base-data)模块target目录下的jar包，执行前保证第一步package执行完成。
<br>initdb.bat文件内容如下，修改数据库连接信息为您本人的。
```batch
java -version
set url=jdbc:postgresql://localhost:5432/postgres
set user=postgres
set password=
set args=-url %url% -user %user% -password %password%
java -classpath "./gaf-cloud/gaf-commons/gaf-common-base-data/target/*;./initdb.jar" com.supermap.gaf.db.Main %args%
```
## 启动后端
### 后端```./gaf-cloud```目录结构(部分)
```batch
gaf-biz
|-- gaf-gis
|   |-- gaf-analysis                分析
|   |-- gaf-data-mgt                数据处理
|   |-- gaf-idesktopx-plugin        桌面插件
|   `-- gaf-map                     地图应用
gaf-commons
|-- gaf-common-base-data            liquibase changeLog数据库脚本      
gaf-ops
|-- gaf-auth
|   |-- gaf-authentication          认证
|   `-- gaf-authority               鉴权
|-- gaf-microservice
|   |-- gaf-microservice-api        文档中心
|   |-- gaf-microservice-conf       配置中心
|   |-- gaf-microservice-gateway    网关
|   |-- gaf-microservice-governance 微服务治理
|   `-- gaf-microservice-rigister   注册中心
|-- gaf-monitor                     监控
|-- gaf-portal                      门户菜单
|-- gaf-storage                     文件存储
`-- gaf-sys-mgt                     系统接口（字典、目录）
```
### 启动微服务
启动GAF微服务和其他spring-cloud项目没有区别，您只需要修改一些配置。
<br>如数据库连接配置、redis连接配置。您可以使用```GAF_ENV_XXX```等环境变量修改数据库连接配置，或者直接修改application.yml、application-local.yml等文件（本地开发建议）。建议您可以直接使用IDEA打开源代码，边观察边启动项目。<br>
您可以在文件```script/deploy/docker/conf/GAF_ENV_CONFIG.env```找到所有的环境变量配置
```yaml
spring:
  datasource:
    driver-class-name: ${GAF_ENV_DATASOURCE_DRIVER:org.postgresql.Driver}
    url: ${GAF_ENV_DATASOURCE_URL:jdbc:postgresql://localhost:5432/postgres}
    username: ${GAF_ENV_DATASOURCE_USERNAME:postgres}
    password: ${GAF_ENV_DATASOURCE_PASSWORD:root}
  redis:
    database: '0'
    host: ${GAF_ENV_REDIS_HOST:localhost}
    port: ${GAF_ENV_REDIS_PORT:6379}
    password: ${GAF_ENV_REDIS_PASSWORD:}
```

* 启动eureka注册中心(开发必须)
* 启动spring config配置中心(可选，如果您使用application-local.yml本地配置的话，建议)
* 启动gaf-portal门户菜单接口(开发必须)
* 启动spring gateway网关(开发必须)
* 启动gaf-authentication认证(开发必须)
* 启动gaf-authority鉴权(开发必须)
* 启动gaf-sys-mgt系统接口（字典、目录）(开发必须)
<br>--------现在您就可以启动前端使用一些基础的功能、或开始开发自定义模块-----------------
* 启动gaf-map地图应用
* 启动gaf-sys-mgt数据治理
* ...其他
> TIP: 启动类在gaf-xx-app模块中，如[gaf-cloud/gaf-biz/gaf-gis/gaf-map/gaf-map-app/src/main/java/com/supermap/gaf/webgis/Application.java](gaf-cloud/gaf-biz/gaf-gis/gaf-map/gaf-map-app/src/main/java/com/supermap/gaf/webgis/Application.java)
### 测试token接口
```bash
curl --location --request POST 'http://localhost:5000/authentication/token?username=sys_admin&password=123456'
```

## 启动前端
### 本地install and link
仅本地首次运行需要
~~~batch
cd gaf-webyarn install
cd common-gaf
yarn install --update-checksums
yarn link
cd ..
cd common-webapp
yarn install --update-checksums
yarn link
cd ..
cd common-mapapp
yarn install --update-checksums
yarn link
cd ..
cd gaf-webapp
yarn link common-gaf
yarn link common-webbase
yarn link common-mapapp
yarn install --update-checksums
~~~
### 编辑代理文件
[gaf-web/gaf-webapp/project.config.json](gaf-web/gaf-webapp/project.config.json)
~~~javascript
{
    "token": "xxxxxx",
    "proxy": {
        "/api/": {
            "target": "http://localhost:5000/",
            "pathRewrite": {
                "^/api/": ""
            },
            "cookieDomainRewrite": "*",
            "cookiePathRewrite": "/",
            "startsWith": "/"
        }
    }
}
~~~
### dev启动
~~~bash
## workdir: ./gaf-web
yarn dev
~~~